﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StoryController : MonoBehaviour
{
    private Timer timer;
    public float SlideTime;
    public Sprite[] StoryImages;
    public GameObject ImageObject;
    public string NextSceneName;
    private Image image;
    private int currentSlideIndex = 0;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1f;
        timer = new Timer(SlideTime);

        // set the first image
        image = ImageObject.GetComponent<Image>();
        image.sprite = StoryImages[currentSlideIndex];
        currentSlideIndex++;
    }

    // Update is called once per frame
    void Update()
    {
        bool IsEnd = timer.UpdateTimer(Time.deltaTime);
        if (IsEnd)
        {
            //  change image
            if (currentSlideIndex < StoryImages.Length)
            {
                image.sprite = StoryImages[currentSlideIndex];
                currentSlideIndex++;
                // reset timer
                timer = new Timer(SlideTime);
            }
            else
            {
                LoadNextLevel();
            }

        }
    }

    private void LoadNextLevel()
    {
        SceneManager.LoadScene(NextSceneName, LoadSceneMode.Single);
    }
}
