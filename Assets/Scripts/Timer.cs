using System;
using UnityEngine;
class Timer
{
  private float StartTime;
  private float EndTime;
  private float CurrentTime;
  public Timer(float endTime, float startTime = 0f)
  {
    StartTime = startTime;
    CurrentTime = StartTime;
    EndTime = endTime;
  }

  public bool UpdateTimer(float deltaTime)
  {
    CurrentTime += deltaTime;
    Debug.Log(CurrentTime);
    return CurrentTime >= EndTime;
  }
}