﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEditor;

[CreateAssetMenu(fileName = "Data", menuName = "Stats", order = 1)]
public class GameDefaults : ScriptableObject
{
    public string playerName = "PlayerName";
    public int levelCount;
    public int souls;
    public int currentLevel;
    public Vector3[] spawnPoints;

    public Potion[] potions;

}

[Serializable]
public class Potion
{
    public string Type;
    public int Price;
    public int Index;
}