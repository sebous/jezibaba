﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;


public class EnemyBasicBehavior : MonoBehaviour
{

    public LayerMask layerMask;
    private Enemy _enemy;
    private float degrees = 180;
    public float timeToRotate;
    public float waitTime;
    public float ViewDistance;
    [SerializeField] // attach line renderer in inspector (object is SIGHT)
    private GameObject linerendererGO;
    private LineRenderer linerenderer;

    [Header("EFFECTS SETTINGS")]

    public float EnemySpeedStep;
    private bool canDetect = true;



    void Start()
    {
        Time.timeScale = 1;
        Transform mytransform = linerendererGO.transform;
        Sequence sequence = DOTween.Sequence(); // create a sequencez
        sequence.Append(mytransform.DOLocalRotate(new Vector3(0f, 0f, degrees), timeToRotate).SetDelay(waitTime)); // rotate it by 90 on X axis for 1 second, and delay it for 3 seconds

        sequence.Append(mytransform.DOLocalRotate(new Vector3(0f, 0f, 0), timeToRotate).SetDelay(waitTime)); // rotate it by 90 on X axis for 1 second, and delay it for 3 seconds
        sequence.Append(mytransform.DOLocalRotate(new Vector3(0f, 0f, -degrees), timeToRotate).SetDelay(waitTime)); // rotate it by 90 on X axis for 1 second, and delay it for 3 seconds
        sequence.Append(mytransform.DOLocalRotate(new Vector3(0f, 0f, 0), timeToRotate).SetDelay(waitTime)); // rotate it by 90 on X axis for 1 second, and delay it for 3 seconds

        sequence.SetLoops(-1);
        linerenderer = linerendererGO.GetComponent<LineRenderer>();

    }


    void Awake()
    {

        _enemy = this.transform.parent.GetComponentInParent<Enemy>();

    }



    void FixedUpdate()
    {

        RaycastHit2D hit = Physics2D.Raycast(linerendererGO.transform.position, linerendererGO.transform.right, ViewDistance, 1 << LayerMask.NameToLayer("Player"));
        Ray2D ray = new Ray2D(linerendererGO.transform.position, linerendererGO.transform.right);
        Debug.DrawRay(linerendererGO.transform.position, linerendererGO.transform.right, Color.green);
        linerenderer.SetPosition(0, ray.origin);
        linerenderer.SetPosition(1, ray.GetPoint(ViewDistance));


        if (hit.collider != null)
        {

            if (hit.collider.name == "player" && canDetect)
            {
                GameObject player = hit.collider.gameObject;
                // dead
                /*
                Vector3 currentScale = this.transform.localScale;
                this.transform.DOPunchScale(new Vector3(transform.localScale.x * 1.25f, transform.localScale.y * 1.25f, 0),0.1f, 10);
                this.transform.DOScale(currentScale,0.1f);
                */
                _enemy.speed += EnemySpeedStep;

                StartCoroutine(DeactivateDetection(player));
                Debug.Log("SPEED");
            }
        }




    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        other.gameObject.GetComponent<PlayerController>().StartCoroutine("PlayerGotCaught");
        LevelManager.Instance.ResetPoints();
        LevelManager.Instance.RestartLevel();
        Debug.Log("KILLLLLL");

    }

    IEnumerator DeactivateDetection(GameObject player)
    {
        canDetect = false;
        Time.timeScale = 0.5f;
        yield return new WaitForSeconds(0.1f);
        Time.timeScale = 1;
        this.transform.DOPunchScale(new Vector3(transform.localScale.x * 0.5f, transform.localScale.y * 0.5f, 0), 0.3f, 10);
        //  player.GetComponent<PlayerController>().StartCoroutine("PlayerGotCaught");

        yield return new WaitForSeconds(0.75f);

        canDetect = true;
    }





}



