﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Linq;
using UnityEngine.SceneManagement;

public class ShopController : MonoBehaviour
{
    public GameDefaults GameData;
    private Text ScoreText;

    // Start is called before the first frame update
    void Start()
    {
        //   show total points
        ScoreText = GameObject.FindGameObjectWithTag("Score").GetComponent<Text>();
        //  ScoreText.text = GameStats.CurrentPoints.ToString();
        ScoreText.text = GameData.souls.ToString();
    }

    public void ClickHandler(string s)
    {
        int potionIndex = int.Parse(s);

        Potion clickedPotion = GameData.potions.First(p => p.Index == potionIndex);
        if (GameStats.CurrentPoints >= clickedPotion.Price && GameStats.PlayerEffects.Count < 3)
        {
            //   add score, effects
            GameStats.CurrentPoints -= clickedPotion.Price;
            GameStats.PlayerEffects.Add(clickedPotion);
            ScoreText.text = GameStats.CurrentPoints.ToString();

            GameObject[] potions = GameObject.FindGameObjectsWithTag("Potion");
            potions[potionIndex].GetComponent<Image>().enabled = false;

            GameObject[] itemSlots = GameObject.FindGameObjectsWithTag("InventorySlot");

            Image targetSlot = itemSlots[GameStats.PlayerEffects.Count - 1].GetComponent<Image>();
            targetSlot.sprite = potions[potionIndex].GetComponent<Image>().sprite;
        }

    }

    public void ExitShop()
    {
        SceneManager.LoadScene("Level " + GameStats.CurrentLevel.ToString());
    }
}
