﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Detector : MonoBehaviour
{
    public Collider2D Collider;
    public float ViewAngle;
    public float ViewDistance;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
     
    }

    void FixedUpdate()
    {
        for (float angle = -ViewAngle; angle <= ViewAngle; angle++)
        {
            Vector3 baseView = new Vector3(ViewDistance, 0, 0);
            Vector3 angleVector = Quaternion.Euler(0, 0, angle) * baseView;

            // calculate if collides with player
            RaycastHit2D hit = Physics2D.Raycast(transform.position, angleVector, ViewDistance);
            if (hit.collider != null)
            {
                if (hit.collider.name == "player")
                {
                    // dead
                    Debug.Log("Hit");
                }
            }

        }
    }



    void OnDrawGizmosSelected()
    {
        float totalFOV = ViewAngle;
        float rayRange = ViewDistance;
        float halfFOV = ViewAngle / 2.0f;
        Quaternion leftRayRotation = Quaternion.AngleAxis(-halfFOV, Vector3.up);
        Quaternion rightRayRotation = Quaternion.AngleAxis(halfFOV, Vector3.up);
        Vector3 leftRayDirection = leftRayRotation * transform.forward;
        Vector3 rightRayDirection = rightRayRotation * transform.forward;
        Gizmos.DrawRay(transform.position, leftRayDirection * rayRange);
        Gizmos.DrawRay(transform.position, rightRayDirection * rayRange);
    }
}
