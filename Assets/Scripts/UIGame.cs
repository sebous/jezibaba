﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGame : MonoBehaviour
{

    public static UIGame Instance { get; private set; }
    public Canvas _canvas;
    public Image _soulsCounterImage;
    public Text _soulsCounterText;
    public GameDefaults _playerStats;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        _soulsCounterText.text = _playerStats.souls.ToString();
    }
}
