﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//moves object along a series of waypoints
//this class adds a kinematic rigidbody, can't figure how to do the same with rigidbody2d as never used it
//the waypoints need to be tagged as "Waypoints" 
//The object cointaining this script should be only the "moving thing" Enemy logic should be in a child object
//with the sprite renderer
[RequireComponent(typeof(Rigidbody))]
public class Enemy : MonoBehaviour
{
    [Header("WAYPOINT BEHAVIOR")]
    public float speed;          //how fast to move
    public float delay;          //how long to wait at each waypoint
    public type movementType;        //stop at final waypoint, loop through waypoints or move back n forth along waypoints

    public enum type { PlayOnce, Loop, PingPong }
    private int currentWp;
    private float arrivalTime;
    private bool forward = true, arrived = false;
    private List<Transform> waypoints = new List<Transform>();
    public Transform waypointParent;

    private float oldPosition = 0.0f;

    private bool isLookingRight;

    [Header("ATTACH HERE ENEMY SPRITE!!!")]
    public SpriteRenderer enemySpriteRenderer;

    void Awake()
    {
        //add kinematic rigidbody
        if (!GetComponent<Rigidbody>())
            gameObject.AddComponent<Rigidbody>();
        GetComponent<Rigidbody>().isKinematic = true;
        GetComponent<Rigidbody>().useGravity = false;
        GetComponent<Rigidbody>().interpolation = RigidbodyInterpolation.Interpolate;

        //get child waypoints, then detach them (so object can move without moving waypoints)
        foreach (Transform child in waypointParent)
            if (child.tag == "Waypoint")
                waypoints.Add(child);
        if (waypoints.Count == 0)
            Debug.LogError("No waypoints found for 'MoveToPoints' script. To add waypoints: add child gameObjects with the tag 'Waypoint'", transform);
        waypointParent.DetachChildren();
    }

    void Start()
    {
        oldPosition = transform.position.x;
    }


    //if we've arrived at waypoint, get the next one
    void Update()
    {
        if (waypoints.Count > 0)
        {
            if (!arrived)
            {
                if (Vector3.Distance(transform.position, waypoints[currentWp].position) < 0.3f)
                {
                    arrivalTime = Time.time;
                    arrived = true;
                }
            }
            else
            {
                if (Time.time > arrivalTime + delay)
                {
                    GetNextWP();
                    arrived = false;
                }
            }
        }


        if (transform.position.x > oldPosition) 
        {
            isLookingRight = true;
           // transform.localRotation = Quaternion.Euler(0, 0, 0);
            Debug.Log("he's looking right "+ isLookingRight);
            
            
        }

        if (transform.position.x < oldPosition) // he's looking left
        {
            //  transform.localRotation = Quaternion.Euler(0, 180, 0);
            isLookingRight = false;
            Debug.Log("he's looking left "+ isLookingRight);
        }

        if (isLookingRight)
        {
            enemySpriteRenderer.flipX = false;
        }

        else

        {
            enemySpriteRenderer.flipX = true;

        }

        oldPosition = transform.position.x; // update the variable with the new position so we can chack against it next frame

    }

    //move object toward waypoint
    void FixedUpdate()
    {
        if (!arrived && waypoints.Count > 0)
        {
            Vector3 direction = waypoints[currentWp].position - transform.position;
            GetComponent<Rigidbody>().MovePosition(transform.position + (direction.normalized * speed * Time.fixedDeltaTime));
        }
    }

    //get the next waypoint
    private void GetNextWP()
    {
        if (movementType == type.PlayOnce)
        {
            currentWp++;
            if (currentWp == waypoints.Count)
                enabled = false;
        }

        if (movementType == type.Loop)
            currentWp = (currentWp == waypoints.Count - 1) ? 0 : currentWp += 1;

        if (movementType == type.PingPong)
        {
            if (currentWp == waypoints.Count - 1)
                forward = false;
            else if (currentWp == 0)
                forward = true;
            currentWp = (forward) ? currentWp += 1 : currentWp -= 1;
        }
    }

    //draw gizmo spheres for waypoints
    void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        foreach (Transform child in transform)
        {
            if (child.tag == "Waypoint")
                Gizmos.DrawSphere(child.position, .7f);
        }
    }
}






