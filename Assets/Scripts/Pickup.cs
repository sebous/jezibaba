﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
  public float RotateSpeed;
  // Start is called before the first frame update
  void Start()
  {
  }

  // Update is called once per frame
  void Update()
  {
    transform.Rotate(new Vector3(0, 0, 45) * Time.deltaTime * RotateSpeed);
  }

  private void OnCollisionEnter2D(Collision2D other)
  {

    if (other.gameObject.name == "player" && enabled)
    {
      LevelManager.Instance.AddPoints(1);
      LevelManager.Instance.ScalePlayer();
      Destroy(gameObject);
    }
  }
}
