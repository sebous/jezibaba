﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance { get; private set; }

    public GameDefaults GameData;
    private float levelPlayerSize;
    public float PlayerScaleStep;

    private void Awake()
    {
        Time.timeScale = 1f;
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void AddPoints(int num)
    {
        GameStats.CurrentPoints += num;
        GameData.souls += num;
    }

    public void SubtractPoints(int num)
    {
        GameStats.CurrentPoints -= num;
        GameData.souls -= num;
    }

    public void ResetPoints()
    {
        GameStats.CurrentPoints = 0;
        GameData.souls = 0;
    }


    public void ScalePlayer()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        player.transform.localScale += new Vector3(PlayerScaleStep, PlayerScaleStep, 0);
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
    }
    public void FinishLevel(string nextStoryName)
    {
        Debug.Log(GameStats.CurrentLevel);

        // remove all effects
        GameStats.PlayerEffects.Clear();

        //  go to next level 
        if (GameStats.CurrentLevel < GameData.levelCount)
        {
            GameStats.CurrentLevel += 1;
            if (!string.IsNullOrEmpty(nextStoryName))
            {
                //  if should redirect to story
                GameStats.ExtraStoryName = nextStoryName;
            }
            else
            {
                GameStats.ExtraStoryName = string.Empty;
            }
            // load shop
            SceneManager.LoadScene("ShopBetweenLevels", LoadSceneMode.Single);

        }

        // You've reached the last level the game is finished
        else
        {
            SceneManager.LoadScene("GameFinished", LoadSceneMode.Single);
        }
    }
}