﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "Data", menuName = "EnemyName", order = 2)]
public class EnemyStats : ScriptableObject
{
    public string EnemyName = "EnemyName";
    public float waypointSpeed;
    public float waypointDelay;

    public float originalSize;
    public float effectSize;

    public float originalSpeed;
    public float effectSpeed;
    
}