﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEditor;

public class LevelEndController : MonoBehaviour
{
    public string NextStoryName;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player" && enabled)
        {
            LevelManager.Instance.FinishLevel(NextStoryName);
            Destroy(gameObject);
        }
    }
}
