﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerController : MonoBehaviour
{
    /// <summary>
    /// Tank movement 
    /// This is attached to the parent GO (game Object) where the logic is attached. 
    /// A child object contains the Sprite that is fixed and won't rotate (logic for this behavior)
    /// commented down
    /// </summary>
    /// 

    private SpriteRenderer _playerSprite;

    public float RotateSpeed, speed;
    public GameObject spriteGO; // gameobject holding the sprite renderer
    private float dx = 5, dy = 5;
    private Rigidbody2D rigidBody;
    private Transform thisTransform;

    private bool isDead;

    private Quaternion spriteRot, thisRot; //initial rotation of the GO holding the sprite renderer


   

    void Awake()
    {

        thisTransform = this.thisTransform;

        _playerSprite = GetComponentInChildren<SpriteRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
       
        Time.timeScale = 1;
        rigidBody = GetComponent<Rigidbody2D>();
        spriteRot = spriteGO.transform.rotation; //initial roation or sprite
        thisRot = this.transform.rotation;


          

    }

    void Update()
    {
        // float rotate = Input.GetAxis("Horizontal");
        //   transform.Rotate(0, 0,  rotate * Time.deltaTime * RotateSpeed );

        float movement = Input.GetAxis("Vertical");

        dx = Input.GetAxis("Horizontal");
        dy = Input.GetAxis("Vertical");

        if (dx <0)
        {
            _playerSprite.flipX = true;
            Debug.Log("he's looking left ");
        }

        if (dx > 0)
        {
            _playerSprite.flipX = false;
            Debug.Log("he's looking right ");
        }



    }
    void FixedUpdate()
    {

        if (dy > 0)
        {
            transform.Translate(Vector2.up * speed);


        }


        if (dy < 0)

        {
            transform.Translate(Vector2.down * speed);


        }

        if (dx > 0)
        {
            transform.Translate(Vector2.right * speed);


        }


        if (dx < 0)

        {
            transform.Translate(Vector2.left * speed);


        }
    }

    void LateUpdate()
    {

        spriteGO.transform.rotation = spriteRot; // logic for preventing the sprite from rotating
        transform.rotation = thisRot;
    }



    IEnumerator PlayerGotCaught()
    {

        Material material = _playerSprite.material;
        Time.timeScale = 0.4f;
        material.DOColor(Color.red, 0.5f);

        yield return new WaitForSeconds(0.3f);

        Time.timeScale = 0.1f;

        yield return new WaitForSeconds(0.3f);
       
        Time.timeScale = 0f;
        yield return new WaitForSeconds(0.1f);
        Time.timeScale = 1f;
        LevelManager.Instance.RestartLevel();
        Destroy(gameObject);



    }
}
