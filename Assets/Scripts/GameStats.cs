
using System.Collections.Generic;
using UnityEngine;

public class GameStats : MonoBehaviour
{
    // public variables
    public int CurrentLevel = 1;
    public int CurrentPoints = 0;
    public float PlayerSpeed = 1f;
    public float EnemySpeed = 1f;
    public float PlayerSize = 1f;
    public List<Potion> PlayerEffects = new List<Potion>();
    public string ExtraStoryName = string.Empty;

    public static GameStats Instance { get; private set; }
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}

// using System.Collections.Generic;

// public static class GameStats
// {
//     public static int CurrentLevel = 1;
//     public static int CurrentPoints = 9;
//     public static float PlayerSpeed = 1f;
//     public static float EnemySpeed = 1f;
//     public static float PlayerSize = 1f;
//     public static List<Potion> PlayerEffects = new List<Potion>();
//     public static string ExtraStoryName = string.Empty;

// }

